# Template: Red Hat SSO 7.3 + Postgresql (Persistent) HA

## Description

Red Hat SSO based on RH-SSO 7.3 image. Transformed from template https://github.com/jboss-container-images/redhat-sso-7-openshift-image/blob/sso73-dev/templates/sso73-x509-postgresql-persistent.json
This project deploys a HA SSO with 3 pods. It using a postgresql database with a persistent volume. 
The route is reencrypt.

## Parameters

| Name                          | Display-Name                              | Required | Description                                                             | Default-Value                       |
|-------------------------------|-------------------------------------------|:--------:|-------------------------------------------------------------------------|:-----------------------------------:|
| `replicaCount`                | Number of replicas                        | true     | Number of replicas for the RH-SSO pods                                  | `3`                                 |
| `ssoHostname`                 | Custom RH-SSO Server Hostname             | false    | Custom hostname for the RH-SSO server                                   | |
| `jgroupsClusterPassword`      | JGroups Cluster                           | true     | The password for the JGroups cluster                                    | |
| `dbJndi`                      | Database JNDI Name                        | false    | Database JNDI name used by application to resolve the datasource        | `java:jboss/datasources/postgresql` |
| `dbDatabase`                  | Database Name                             | true     | Database name                                                           | `root`                              |
| `dbMinPoolSize`               | Datasource Minimum Pool Size              | false    | Sets xa-pool/min-pool-size for the configured datasource                | |
| `dbMaxPoolSize`               | Datasource Maximum Pool Size              | false    | Sets xa-pool/max-pool-size for the configured datasource                | |
| `dbTxIsolation`               | Datasource Transaction Isolation          | false    | Sets transaction-isolation for the configured datasource                | |
| `postgresqlMaxConnections`    | PostgreSQL Maximum number of connections  | false    | The maximum number of client connections allowed and number of prepared transactions |  |
| `postgresqlSharedBuffers`     | PostgreSQL Shared Buffers                 | false    | Configures how much memory is dedicated to PostgreSQL for caching data  | |
| `dbUsername`                  | Database Username                         | true     | Database user name                                                      | |
| `dbPassword`                  | Database Password                         | true     | Database user password                                                  | |
| `dbVolumeCapacity`            | Database Volume Capacity                  | false    | Size of persistent storage for database volume                          | `1Gi`                               |
| `image.imageStreamNamespace`  | ImageStream Namespace                     | true     | Namespace in which the ImageStreams for Red Hat Middleware images are installed | `openshift`                 |
| `image.sso.image`             | SSO-image                                 | true     | RH-SSO image name                                                       | `redhat-sso73-openshift`            |
| `image.sso.tag`               | SSO-image tag                             | true     | RH-SSO image tag                                                        | `1.0`                               |
| `image.sso.pullPolicy`        | SSO-image pull policy                     | true     | Pull policy of the RH-SSO image                                         | `Always`                            |
| `image.postgresql.image`      | Postgresql-image                          | true     | Postgresql image name                                                   | `postgresql`                        |
| `image.postgresql.tag`        | Postgresql-image tag                      | true     | Postgresql image tag                                                    | `9.5`                               |
| `image.postgresql.pullPolicy` | Postgresql-image pull policy              | true     | Pull policy of the Postgresql image                                     | `Always`                            |
| `ssoRealm`                    | RH-SSO Realm                              | false    | Realm to be created in the RH-SSO server                                | `Openshift`                         |
| `ssoServiceUsername`          | RH-SSO Service Username                   | false    | The username used to access the RH-SSO service used by clients to create the application clients |  |
| `ssoServicePassword`          | RH-SSO Service Password                   | false    | The password for the RH-SSO service user                                |  |
| `ssoAdminUsername`            | RH-SSO Admin Username                     | true     | Admin name of the RH-SSO-Server                                         |  |
| `ssoAdminPassword`            | RH-SSO Admin Username                     | true     | Admin passowrd of the RH-SSO-Server                                     |  |
| `memoryLimit`                 | Container Memory Limit                    | false    | Container memory limit                                                  | `1Gi`                               |

## Installing the Chart with Helm 3

To install the chart with the release name `sso`:

```bash
$ helm install sso local_git_repo
```

## Uninstalling the Chart

To uninstall/delete the `sso` deployment:

```bash
$ helm uninstall tomcat
```

The command removes all the Kubernetes components associated with the chart and deletes the release.
