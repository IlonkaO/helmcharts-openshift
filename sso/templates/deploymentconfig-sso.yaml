kind: DeploymentConfig
apiVersion: apps.openshift.io/v1
metadata:
  name: {{ template "sso.fullname" . }}
  labels:
    application: {{ template "sso.fullname" . }}
    chart: {{ .Chart.Name }}-{{ .Chart.Version | replace "+" "_" }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
spec:
  replicas: {{ .Values.replicaCount }}
  updateStrategy:
    type: {{ .Values.strategy.type }}
  triggers: 
  - type: ImageChange
    imageChangeParams: 
      automatic: true
      containerNames: 
       - {{ template "sso.fullname" . }}
      from: 
       kind: ImageStreamTag
       namespace: {{ .Values.image.imageStreamNamespace }}
       name: {{ .Values.image.sso.image }}:{{ .Values.image.sso.tag }}
  - type: ConfigChange
  selector:
    deploymentConfig: {{ template "sso.fullname" . }}
  template:
    metadata:
      name: {{ template "sso.fullname" . }}
      labels:
        deploymentConfig: {{ template "sso.fullname" . }}
        application: {{ template "sso.fullname" . }}
    spec:
      terminationGracePeriodSeconds: 75
      affinity: 
        nodeAffinity: 
          requiredDuringSchedulingIgnoredDuringExecution: 
          - nodeSelectorTerms: 
              matchExpressions: 
              - key: region
                operator: In
                values: infra
        podAntiAffinity: 
          requiredDuringSchedulingIgnoredDuringExecution: 
          - labelSelector: 
              matchExpressions: 
              - key: name
                operator: In
                values: {{ template "sso.name" . }}
      containers:
      - name: {{ template "sso.fullname" . }}
        image: {{ .Values.image.sso.image }}:{{ .Values.image.sso.tag }}
        imagePullPolicy: {{ .Values.image.sso.pullPolicy }}
        resources:
          limits:
            memory: {{ .Values.memoryLimit }}
        volumeMounts:
        - name: "sso-x509-https-volume"
          mountPath: "/etc/x509/https"
          readOnly: true
        - name: "sso-x509-jgroups-volume"
          mountPath: "/etc/x509/jgroups"
          readOnly: true
        livenessProbe:
          exec:
            command:
            - "/bin/bash"
            - "-c"
            - "/opt/eap/bin/livenessProbe.sh"
          initialDelaySeconds: 60
        readinessProbe:
          exec:
            command:
            - "/bin/bash"
            - "-c"
            - "/opt/eap/bin/readinessProbe.sh"
        ports:
        - name: jolokia
          containerPort: 8778
          protocol: TCP
        - name: http
          containerPort: 8080
          protocol: TCP
        - name: https
          containerPort: 8443
          protocol: TCP
        - name: ping
          containerPort: 8888
          protocol: TCP
        env:
        - name: SSO_HOSTNAME
          value: {{ .Values.ssoHostname }}
        - name: DB_SERVICE_PREFIX_MAPPING
          value: {{ template "sso.fullname" . }}-postgresql=DB
        - name: DB_JNDI
          value: {{ .Values.database.dbJndi }}
        - name: DB_USERNAME
          value: {{ .Values.database.dbUsername }}
        - name: DB_PASSWORD
          value: {{ .Values.database.dbPassword }}
        - name: DB_DATABASE
          value: {{ .Values.database.dbDatabase}}
        - name: TX_DATABASE_PREFIX_MAPPING
          value: {{ template "sso.fullname" . }}-postgresql=DB
        - name: DB_MIN_POOL_SIZE
          value: {{ .Values.database.dbMinPoolSize }}
        - name: DB_MAX_POOL_SIZE
          value: {{ .Values.database.dbMaxPoolSize }}
        - name: DB_TX_ISOLATION
          value: {{ .Values.database.dbTxIsolation }}
        - name: JGROUPS_PING_PROTOCOL
          value: openshift.DNS_PING
        - name: OPENSHIFT_DNS_PING_SERVICE_NAME
          value: {{ template "sso.fullname" . }}-ping
        - name: OPENSHIFT_DNS_PING_SERVICE_PORT
          value: '8888'
        - name: X509_CA_BUNDLE
          value: "/var/run/secrets/kubernetes.io/serviceaccount/service-ca.crt"
        - name: JGROUPS_CLUSTER_PASSWORD
          value: {{ .Values.jgroups.clusterPassword }}
        - name: SSO_ADMIN_USERNAME
          valueFrom: 
            secretKeyRef: 
              name: "secret-sso-admin"
              key: username
        - name: SSO_ADMIN_PASSWORD
          valueFrom: 
            secretKeyRef: 
              name: "secret-sso-admin"
              key: password
        - name: SSO_REALM
          value: {{ .Values.ssoServer.ssoRealm }}
        - name: SSO_SERVICE_USERNAME
          value: {{ .Values.ssoServer.ssoServiceUsername }}
        - name: SSO_SERVICE_PASSWORD
          value: {{ .Values.ssoServer.ssoServicePassword }}
      volumes:
      - name: "sso-x509-https-volume"
        secret:
          secretName: "sso-x509-https-volume"
      - name: "sso-x509-jgroups-volume"
        secret:
          secretName: "sso-x509-jgroups-secret"
